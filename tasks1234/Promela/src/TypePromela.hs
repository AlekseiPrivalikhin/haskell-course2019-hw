module AST where

import Data.Int

data Type =
   IntVal  Int
 | BoolVal Bool
 | BitVal Bool
 | ByteVal Int8
 | MtypeVal Int8
 | ShortVal Int16
 deriving Show

data Exp =     
   Const Type
 | ArrayVal [Exp]
 | Variable String                                              
 | Plus Exp Exp                                                   
 | Minus Exp Exp                                               
 | Mul Exp Exp                                             
 | Div Exp Exp 
 | Greater Exp Exp
 | Less Exp Exp
 | Equal Exp Exp
 | Neg Exp
 | And Exp Exp
 | Or Exp Exp
 deriving Show

data Statement =  
   Assig String Exp
 | Seq [Statement]           
 | If Exp Statement Statement  -- second for else     
 | Loop Statement  --loop can ends only with break or goto
 | Goto Statement
 | Break
 | Skip --ends programm        
 deriving Show

data Chan = Chan { chanName::String, size::Int, oftype::Type }

data Proctype = Proctype { procName::String, chanels::[Chan], procBody::Statement }

data Init = Init { initBody::Statement }