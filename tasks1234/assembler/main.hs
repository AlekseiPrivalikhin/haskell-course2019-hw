{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Main where

import Data.Int
import Data.List
import Data.Maybe

data Essential = Essential { rax
                           , rbx
                           , rcx
                           , rdx
                           , rsi
                           , rdi
                           , rbp
                           , rsp 
                           , rip :: Int64
                           , labels :: [(String, Int64)]
                           , instructions :: [((String, String, String), Int64)]
                           }

data Storage =
    Byte Int8
  | Word Int16
  | Double Int32
  | Quad Int64 
  | Reg String

addInstr :: Essential -> String -> String -> String -> Essential
addInstr ess i o1 o2 = ess { instructions = (((i, o1, o2), rip ess + 1) : instructions ess) 
                           , rip = rip ess + 1 
                           }

removeInstr :: Essential -> String -> String -> String -> Essential
removeInstr ess i o1 o2 = ess { instructions = delete ((i, o1, o2), fromJust (lookup (i, o1, o2) (instructions ess))) (instructions ess) }

get :: Essential -> String -> Int64
get ess s | s == "rax" = rax ess
          | s == "rbx" = rbx ess
          | s == "rcx" = rcx ess
          | s == "rdx" = rdx ess
          | s == "rsi" = rsi ess
          | s == "rdi" = rdi ess
          | s == "rbp" = rbp ess
          | s == "rsp" = rsp ess
          | otherwise = error "Invalid register name"

set :: Essential -> String -> Int64 -> Essential
set ess s n | s == "rax" = ess { rax = n } 
            | s == "rbx" = ess { rbx = n } 
            | s == "rcx" = ess { rcx = n } 
            | s == "rdx" = ess { rdx = n } 
            | s == "rsi" = ess { rsi = n } 
            | s == "rdi" = ess { rdi = n } 
            | s == "rbp" = ess { rbp = n } 
            | s == "rsp" = ess { rsp = n } 
            | otherwise = error "Invalid register name"           

toInt64 :: Essential -> Storage -> Int64
toInt64 ess (Byte n) = fromIntegral n
toInt64 ess (Word n) = fromIntegral n  
toInt64 ess (Double n) = fromIntegral n  
toInt64 ess (Quad n) = fromIntegral n  
toInt64 ess (Reg s) = get ess s  

toStr :: Storage -> String
toStr (Byte n) = show n
toStr (Word n) = show n  
toStr (Double n) = show n  
toStr (Quad n) = show n  
toStr (Reg s) = show s

addLabel :: Essential -> String -> Essential
addLabel ess s = ess { labels = ((s, rip ess + 1) : labels ess) 
                     , rip = rip ess + 1
                     }
 
removeLabel :: Essential -> String -> Essential
removeLabel ess s = ess { labels = delete (s, fromJust (lookup s (labels ess))) (labels ess) }

findLabel :: Essential -> Int
findLabel ess = fromJust (elemIndex (elem (instructions ess) (rip ess)) (instructions ess)) 
  where elem (x:xs) rip = if (snd x) > rip then x else elem xs rip
        elem [] rip = error "Instruction not find"

interpreter :: Essential -> Essential   
interpreter ess = helper ess (drop (findLabel ess - 1) (instructions ess))
  where helper ess [] = ess
        helper ess (x:xs) | fst3 (fst x) == "jmp" = helper (jmp ess (snd3 (fst x))) xs 
                          | fst3 (fst x) == "jbool" = helper (jbool ess (snd3 (fst x)) (read (thd3 (fst x)) :: Bool)) xs
                          | fst3 (fst x) == "mov" = helper (mov ess (snd3 (fst x)) (readStorage (thd3 (fst x)))) xs 
                          | otherwise = error "Invalid instruction"

readStorage :: String -> Storage
readStorage s | "Reg" `isPrefixOf` s = Reg (drop 4 s)
              | "Byte" `isPrefixOf` s = Byte (read (drop 5 s) :: Int8)
              | "Word" `isPrefixOf` s = Word (read (drop 5 s) :: Int16)
              | "Double" `isPrefixOf` s = Double (read (drop 7 s) :: Int32)
              | "Quad" `isPrefixOf` s = Quad (read (drop 5 s) :: Int64)
              | otherwise = error "Invalid operand"

fst3 (a, b, c) = a
snd3 (a, b, c) = a
thd3 (a, b, c) = c

mov :: Essential -> String -> Storage -> Essential
mov ess s stor = addInstr (set ess s (toInt64 ess stor)) "mov" s (toStr stor)

jmp :: Essential -> String -> Essential
jmp ess s = interpreter (addInstr (ess { rip = fromMaybe (rip ess) (lookup s (labels ess)) }) "jmp" s "")

jbool :: Essential -> String -> Bool -> Essential
jbool ess s b = if b then jmp (addInstr ess "jbool" s (show b)) s else (addInstr ess "jbool" s (show b))

int :: Essential -> IO ()
int ess = return () 

main :: IO ()
main = let
  ess = addLabel ess "start:"
    where ess = Essential 0
                          0
                          0
                          0
                          0
                          0
                          0
                          0
                          0
                          []
                          []                              
  in int ess     
