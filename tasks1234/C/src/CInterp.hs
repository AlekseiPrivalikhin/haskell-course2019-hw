module CInterp where

import Data.Function((&))

data CType = CInt { valueInt :: Int }
  | CChar { valueChar :: Char }
  | CBool { valueBool :: Bool }
  | CPtr { valuePtr :: [CType] }
  | CVoid { }
  deriving (Show)

negate' :: CType -> CType 
negate' x = case x of 
  (CInt x) -> CInt (-x)
  (CBool f) -> CBool (not f)
  _ -> = error ("Failed at negating")


type Env = [(String, CType)] --it's not right, 'cuz it can't hold functions, will be fixed someday

generateEnv :: Env
generateEnv = []

bindVar :: String -> Expr -> Env -> Env
bindVar x expr env = case lookup x env of
  Nothing -> (x, (eval expr env)) : env
  _ -> replace x expr env 
    where 
     replace x expr ((var, val'):env') = if x == var 
       then (x, (eval expr env')) : env' 
       else (var, val') : replace x expr env'

{-	   
bindFun :: String -> [String] -> Command -> Env -> Env
bindFun f args comms env = ???
-}

data Expr = Literal CType
  | Var String
  | Expr :-: Expr
  | Expr :+: Expr
  | Expr :*: Expr
  | Expr :/: Expr
  | Expr :^: Expr 
  | Neg Expr
  | Eq Expr Expr
  | Call String [Expr] 
  deriving (Show)
  
eval :: Expr -> Env -> CType
eval expr env = case expr of 
  Literal n ->  n
  x :+: y -> let a = eval x env ;
                 b = eval y env ;
             in (CInt $ (a & valueInt) + (b & valueInt))
  x :-: y -> let a = eval x env ;
                 b = eval y env ;
             in (CInt $ (a & valueInt) - (b & valueInt))
  x :*: y -> let a = eval x env ;
                 b = eval y env ;
             in (CInt $ (a & valueInt) * (b & valueInt))
  x :/: y -> let a = eval x env ;
                 b = eval y env ;
             in if (b & valueInt) /= 0 
                  then (CInt $ div (a & valueInt) (b & valueInt)) 
                  else error ("Failed at division by zero")
  x :^: y -> let a = eval x env ;
                 b = eval y env ;
             in (CInt $ (a & valueInt) + (b & valueInt))
  Neg a -> negate' (eval a env)
  Eq x y -> let a = eval x env ;
                b = eval y env ;
            in if (a & valueInt) == (b & valueInt) then CInt 1 
                                                   else CInt 0
  -- Call fun args -> 
  Var x -> case lookup x env of 
           Nothing -> error ("unbound variable: " ++ x)
           Just val -> val --yes, it only works for variables for now
  _ -> error ("Couldn't evaluate expression: " ++ show expr)
  

data Stmt = Assign String Expr
  | Block [Stmt]
  | Cond Expr Stmt Stmt -- if-then-else condition
  | While Expr Stmt
  | Declare String [String] Stmt --function declaration
  | Ret Expr
  | Print Expr
  deriving (Show)
  
type Prog = Stmt
{-
--needs a monad to be implemented as (statement-output) virtual-machine, i guess
interp :: Stmt -> Env -> Monad' ()
interp stmt env = case stmt of 
  Assign name val -> bindVar name val env
  Seq (st1:st2) -> do { x <- interp st1 env;
                          y <- interp st2 env;
						  return () }
  Cond condition st1 st2 -> do { x <- eval condition env ;
                                   if x == 1 
								    then interp st1 env
								    else interp st2 env }
  While exp st -> do { val <- eval exp env ;
                        if (val & valueInt) == 0 
		                  then return ()
      		              else do {interp exp env ;
			                       interp (While exp st) env } }
  Declare fun args st -> bindFun fun args st
  Ret expr -> -- not yet
  Ret comm -> 
  Print expr -> do { val <- eval expr env ;
                     output val }  
				  				  
output :: Show a => a -> Monad' ()
-}