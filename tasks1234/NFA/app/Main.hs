module Main where

import Lib
import AST
import Parser

main = do
    contents <- readFile "test.txt"
    let a = snd(head( run mainparser contents))
    print a
