module Parser where

import AST
import Control.Monad (void)
import Control.Monad.Combinators.Expr
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

sc :: Parser ()
sc = L.space (skipSome spaceChar) blockCmnt lineCmnt
  where
    blockCmnt = L.skipBlockComment "--[[" "]]"
    lineCmnt  = L.skipLineComment "--"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

parens :: Parser a -> Parser a
parens = (between . try) (symbol "(") (symbol ")")

quotes :: Parser a -> Parser a
quotes x = (between . try) (char '\"') (symbol "\"") x
      <|>  (between . try) (char '\'')  (symbol "'")  x
      <|>  (between . try) (char '[')    (symbol "]")  (helpQ x)

helpQ :: Parser a -> Parser a
helpQ x = (between . try) (char '=')   (char '=') (helpQ x)
      <|> (between . try) (symbol "[") (char ']') x

brackets :: Parser a -> Parser a
brackets = between (symbol "[") (char ']')

braces :: Parser a -> Parser a
braces = between (symbol "{") (symbol "}")

number :: Parser Double
number = (lexeme . try) L.float
     <|> fromIntegral <$> (lexeme L.decimal)

bool' :: Parser Bool
bool' = try (True <$ rword "true") <|> (False <$ rword "false")
     
stringContent :: Parser String
stringContent
  = many (spaceChar
      <|> alphaNumChar
      <|> char '.' 
      <|> char '!' 
      <|> char '?' 
      <|> char ',')

semi :: Parser String
semi = symbol ";"

comma :: Parser String
comma = symbol ","

rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

keywords :: [String]
keywords = [ "and"  , "break", "do"   , "else"    , "elseif", "end"
           , "false", "goto" , "for"  , "function", "if"    , "in"
           , "local", "nil"  , "not"  , "or"      , "repeat", "return"
           , "then" , "true" , "until", "while"                        ]

identifier :: Parser Literal
identifier = tExpr <* sc
                
identifier' :: Parser Literal
identifier' = (p >>= check) where
    p       = (:) <$> (try letterChar <|> char '_') <*> many (try alphaNumChar <|> char '_')
    check x = if x `elem` keywords
        then fail "Illegal name for variable."
        else return (SConst x)

whileParser :: Parser Statement
whileParser = between sc eof stmt

stmt :: Parser Statement
stmt = Block <$> sepBy1 statement' (optional semi)

expression :: Parser Literal
expression = makeExprParser term operator

term :: Parser Literal
term = parens          expression
   <|> try (Call   <$> function'            )
   <|> try (Var    <$> identifier           )
   <|> try (AConst <$> number               )
   <|> try (BConst <$> bool'                )
   <|> try (SConst <$> quotes stringContent )
   <|> Nil         <$  rword "nil"

operator :: [[Operator Parser Literal]]
operator =
    [
      [ Prefix (UnOp Not <$ symbol "-") 
      , Prefix (UnOp Len <$ symbol "#")
      , Prefix (UnOp Not <$ rword  "not") ]
    , 
      [ InfixL (BiOp Con <$ symbol "..")
      , InfixR (BiOp Pow <$ symbol "^")   ]
    , 
      [ InfixL (BiOp Mul <$ symbol "*") 
      , InfixL (BiOp Div <$ symbol "/") 
      , InfixL (BiOp Mod <$ symbol "%")   ]
    , 
      [ InfixL (BiOp Add <$ symbol "+")
      , InfixL (BiOp Sub <$ symbol "-")   ]
    ,
      [ InfixN (BiOp Le  <$ symbol "<=")
      , InfixN (BiOp Lt  <$ symbol "<" )
      , InfixN (BiOp Ge  <$ symbol ">=") 
      , InfixN (BiOp Gt  <$ symbol ">" )  ]
    ,
      [ InfixN (BiOp Eq  <$ symbol "==")
      , InfixN (BiOp Ne  <$ symbol "~=")  ]
    ,
      [ InfixL (BiOp And <$ rword  "and") ]
    ,
      [ InfixL (BiOp Or  <$ rword  "or" ) ]
    ]
   
tExpr :: Parser Literal
tExpr = makeExprParser tTerm tOperator

tTerm :: Parser Literal
tTerm = parens tExpr
    <|> try (do
        name   <- Var <$> identifier'
        nested <- brackets expression
        return (BiOp Fld name nested))
    <|> Var <$> identifier'

tOperator :: [[Operator Parser Literal]]
tOperator = [[ InfixL (BiOp Fld <$ string ".") ]]

statement' :: Parser Statement
statement' = try assign'     <|> try define' <|> try function' <|> try block'  
         <|> try ifThenElse' <|> try for'    <|> try forEach'  <|> try while'   <|> try repeat'
         <|> try local'      <|> try return' <|> try label'    <|> try goto'    <|> try break'

assign' :: Parser Statement
assign' = do
    lvalue <- identifier `sepBy1` comma
    void      (symbol "=")
    rvalue <- ((try (Right <$> (Block <$> (braces (many tassign')))) <|>  (Left <$> expression)))  `sepBy1` comma
    void      (optional semi)
    return (Assign lvalue rvalue)

tassign' :: Parser Statement
tassign' = try (do
    lvalue <- identifier
    void      (symbol "=")
    rvalue <- ((try (Right <$> (Block <$> (braces (many tassign')))) <|>  (Left <$> expression)))
    void (optional comma)
    return (Assign [lvalue] [rvalue]))
  <|> do
    rvalue <- ((try (Right <$> (Block <$> (braces (many tassign')))) <|>  (Left <$> expression)))
    void (optional comma)
    return (Assign [] [rvalue])

define' :: Parser Statement
define' = do
    void (rword "function")
    name <- tExpr
    void (optional sc)
    prms <- parens (identifier' `sepBy` comma)
    body <- statement'
    void (rword "end")
    void (optional semi)
    return (Define name prms body)

function' :: Parser Statement
function' = do
    name <- tExpr
    void (optional sc)
    args <- parens (expression `sepBy` comma)
    return (Function name args)

block' :: Parser Statement
block' = do
    void (rword "do")
    stmt <- many statement'
    void (rword "end")
    void (optional semi)
    return (Block stmt)

ifThenElse' :: Parser Statement
ifThenElse' = do
    void (rword "if")
    cond <- expression
    void (rword "then")
    stmt <- statement'
    othr <- optional (try (rword "elseif" >> elif') <|> (rword "else" >> statement'))
    return (IfThenElse cond stmt othr) 

elif' :: Parser Statement
elif' = do
    cond <- expression
    void (rword "then")
    stmt <- statement'
    othr <- optional (try (rword "elseif" >> (elif' )) <|> (rword "else" >> statement'))
    return (IfThenElse cond stmt othr)

for' :: Parser Statement
for' = do
    void (rword "for")
    var  <- identifier
    void (symbol "=")
    init <- expression
    void comma
    iter <- expression
    void comma
    fin <- expression
    void comma
    step <- expression
    body <- statement'
    return (For var init iter fin body)

forEach' :: Parser Statement
forEach' = do
    void (rword "for")
    var  <- some identifier
    void (rword "in")
    iter <- some expression
    body <- statement'
    return (ForEach var iter body)

while' :: Parser Statement
while' = do
    void (rword "while")
    cond <- expression
    body <- statement'
    return (While cond body)

repeat' :: Parser Statement
repeat' = do
    void (rword "repeat")
    body <- statement'
    void (rword "until")
    cond <- expression
    return (Repeat body cond)

local' :: Parser Statement
local' = do
    void (rword "local")
    exp  <- statement'
    return (Local exp)

return' :: Parser Statement
return' = do
    void (rword "return")
    exp  <- expression
    void (optional semi)
    return (Return exp)

label' :: Parser Statement
label' = do
    void  (symbol "::")
    label <- identifier'
    void  (symbol "::")
    return (AST.Label label)

goto' :: Parser Statement
goto' = do
    void  (rword "goto")
    label <- identifier'
    void  (optional sc)
    void  (optional semi)
    return (Goto label)

break' :: Parser Statement
break' = do
    void (rword "break")
    void (optional semi)
    return Break
