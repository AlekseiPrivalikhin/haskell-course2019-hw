{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}

module Main where

import AST
import Parser
import Text.Megaparsec
import Text.Pretty.Simple (pPrint)
import Text.InterpolatedString.QM (qnb)

main :: IO ()
main = do
    pPrint $ parseMaybe whileParser input
    where
        input  = [qnb|
            --hello, world
            _init = "Hello," .. "World!"

            --[[ factorial ]] res, mul =  1, 1
            limit = read()
            while (mul <= limit) do
                res = res * mul
                mul = mul + 1
            end
            write(res)

            -- table example 
            foobar = {maxInt[0] = N , prettyInt = "42"}

            foobar[maxInt[1]] = N + 1
            foobar.field[123].undefined = nil
        |]
