module AST where


data PNum = Int | Float
		deriving (Show)

data PyTypes = PyNum PNum 
				  | PList [PyTypes]
				  | PChar Char
				  | PString String
				  | PFunction 
				  | PVoid
		deriving (Show)

data BBinOp = And | Or deriving (Show)

data RBinOp = LT | GT | EQ deriving (Show)

data ABinOp = Add
             | Subtract
             | Multiply
             | Divide
               deriving (Show)

data BExpr = BoolConst Bool
            | Not BExpr
            | BBinary BBinOp BExpr BExpr
            | RBinary RBinOp AExpr AExpr
             deriving (Show)

data AExpr = Val PyTypes
            | Neg AExpr
            | ABinary ABinOp AExpr AExpr
              deriving (Show)

data Stmt = Seq [Stmt]
           | VarAssign String AExpr
           | If BExpr Stmt Stmt
           | For BExpr Stmt Stmt
           | While BExpr Stmt
           | Function String [AExpr] Stmt
           | Return AExpr
           | Class String Stmt
             deriving (Show)              


