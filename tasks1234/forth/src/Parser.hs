{-# OPTIONS_GHC -Wall #-}

module Parser where

import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Void
import AST

type Parser = Parsec Void String

ws :: Parser ()
ws = skipSome spaceChar

mws :: Parser ()
mws = skipMany spaceChar

forth :: Parser Forth
forth = sepEndBy forthExpr ws

forthExpr :: Parser Expr
forthExpr = try forthPrim 
        <|> forthLit 
        <|> forthWord
        <|> forthLoop
        <|> forthDef
        <|> forthCond
        <|> forthOutput

forthOutput :: Parser Expr
forthOutput = do
    _ <- char '.'
    return $ Output

forthPrim :: Parser Expr
forthPrim = do
    acc <- choice $ map (string . fst) primOps
    notFollowedBy (digitChar <|> letterChar)
    return $ Prim acc

forthCond :: Parser Expr
forthCond = do
    controlWord "if"
    ws
    thenBody <- forth
    mws
    controlWord "else"
    ws
    elseBody <- forth
    mws
    controlWord "then"
    return $ Cond thenBody elseBody
    
forthDef :: Parser Expr
forthDef = do
    _ <- char ':'
    ws
    name <- wordName
    ws
    body <- forth
    mws
    _ <- char ';'
    return $ Def name body

forthWord :: Parser Expr
forthWord = do
    name <- wordName
    return $ Word name

wordName :: Parser String
wordName = do
    acc <- lookAhead $ some letterChar
    if acc `elem` controls
        then fail "Prohibited word name"
        else (some letterChar)

forthLoop :: Parser Expr
forthLoop = do
    controlWord "do"
    ws
    body <- forth
    mws
    controlWord "loop"
    return $ Loop body

forthLit :: Parser Expr
forthLit = Push <$> signedInteger

signedInteger :: Parser Integer
signedInteger = integer
        <|> char '+' *> integer
        <|> char '-' *> (negate <$> integer)

integer :: Parser Integer
integer = read <$> some digitChar

controlWord :: String -> Parser ()
controlWord name = do 
    _ <- string name
    notFollowedBy letterChar
