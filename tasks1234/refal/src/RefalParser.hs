module RefalParser (refalScope) where

import RefalAst

import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

refalScope :: Parser (Scope Function)
refalScope = space >> many function

function :: Parser (Name, Function)
function = do
    n <- identifier
    b <- block
    return $ (n, Defined b)

block :: Parser Block
block = between blockBegin blockEnd $ endBy sentence symbolOtherwise

sentence :: Parser Sentence
sentence = do
    p <- expression unbound
    cs <- many condition
    s <- substitution
    return $ Sentence p cs s

condition :: Parser Condition
condition = do
    _ <- symbolWhere
    e <- expression evaluable
    _ <- symbolIs
    p <- expression unbound
    return $ Condition e p

substitution :: Parser Substitution
substitution = expressionSubstitution <|> blockApplicationSubstitution

expressionSubstitution :: Parser Substitution
expressionSubstitution = fmap ExpressionSubstitution $ symbolReplaceBy >> expression evaluable

blockApplicationSubstitution :: Parser Substitution
blockApplicationSubstitution = do
    _ <- symbolWith
    e <- expression evaluable
    _ <- symbolIs
    b <- block
    return $ BlockSubstitution b e

expression :: Parser [a] -> Parser (Expression a)
expression p = do
    tss <- many $ (fmap (fmap Term) p) <|> fmap (:[]) (structure p)
    return $ concat tss

structure :: Parser [a] -> Parser (Term a)
structure p = do
    e <- between structureBegin structureEnd $ expression p
    return $ Structure e

unbound :: Parser [Unbound]
unbound = fmap (fmap Unbound) characters <|> fmap (:[]) unboundTerm

unboundTerm :: Parser Unbound
unboundTerm = try unboundVariable <|> fmap Unbound refalSymbol

evaluable :: Parser [Evaluable]
evaluable = fmap (fmap Evaluable) unbound <|> fmap (:[]) evaluableTerm

evaluableTerm :: Parser Evaluable
evaluableTerm = fmap Evaluable unboundTerm <|> functionApplication

functionApplication :: Parser Evaluable
functionApplication = between evaluationBegin evaluationEnd $ do
    n <- identifier
    e <- expression evaluable
    return $ FunctionApplication n e

unboundVariable :: Parser Unbound
unboundVariable = do
    t <- refalType
    _ <- symbolIndexFollows
    n <- identifier
    return $ UnboundVariable n t

refalSymbol :: Parser Symbol
refalSymbol = try (fmap Number number)
    <|> try (fmap RealNumber realNumber)
    <|> try (fmap Identifier identifier)
    <|> fmap Character nonSpecialCharacter

refalType :: Parser Type
refalType = typeParser "s" SymbolType
    <|> typeParser "t" TermType
    <|> typeParser "e" ExpressionType

typeParser :: String -> Type -> Parser Type
typeParser s t = symbol s >> return t

characters :: Parser [Symbol]
characters = lexeme $ fmap (fmap Character) $ literal '\'' <|> literal '"'

identifier :: Parser String
identifier = lexeme $ do
    h <- alphaNumChar
    t <- many $ alphaNumChar <|> char '-' <|> char '_'
    return (h : t)

specialCharacters :: [Char]
specialCharacters = ['(', ')', ',', '&', '\'', '"', '=', '{', '}', '.', '<', '>', ':']

nonSpecialCharacter :: Parser Char
nonSpecialCharacter = lexeme $ do
    c <- lookAhead symbolChar
    if elem c specialCharacters then
        fail ("Invalid use of special character " ++ show c)
    else
        symbolChar

symbolReplaceBy :: Parser String
symbolReplaceBy = symbol "="

symbolOtherwise :: Parser String
symbolOtherwise = symbol ";"

symbolIs :: Parser String
symbolIs = symbol ":"

symbolWhere :: Parser String
symbolWhere = symbol "&"

symbolWith :: Parser String
symbolWith = symbol ","

symbolIndexFollows :: Parser String
symbolIndexFollows = symbol "."

blockBegin :: Parser String
blockBegin = symbol "{"

blockEnd :: Parser String
blockEnd = symbol "}"

evaluationBegin :: Parser String
evaluationBegin = symbol "<"

evaluationEnd :: Parser String
evaluationEnd = symbol ">"

structureBegin :: Parser String
structureBegin = symbol "("

structureEnd :: Parser String
structureEnd = symbol ")"

literal :: Char -> Parser String
literal c = (char c) >> manyTill L.charLiteral (char c)

number :: Parser Integer
number = lexeme $ L.decimal

realNumber :: Parser Double
realNumber = lexeme $ L.float

lexeme :: Parser a -> Parser a
lexeme = L.lexeme space

symbol :: String -> Parser String
symbol = L.symbol space
