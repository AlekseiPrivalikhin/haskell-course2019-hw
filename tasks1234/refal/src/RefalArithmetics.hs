module RefalArithmetics (arithmeticFuns) where

import RefalAst

import Control.Monad.Except

import Data.List

arithmeticFuns :: Scope Function
arithmeticFuns = [ ("Add", createFun $ operator (+))
                 , ("Sub", createFun $ operator (-))
                 , ("Mul", createFun $ operator (*))
                 , ("Div", createFun $ operator (div))
                 , ("Mod", createFun $ operator (mod))
                 , ("Divmod", createFun divmod)
                 , ("Compare", createFun RefalArithmetics.compare)
                 ]

createFun :: (Integer -> Integer -> Value) -> Function
createFun f = BuiltIn $ \expr -> takeArgs expr >>= (return . uncurry f)

operator :: (Integer -> Integer -> Integer) -> Integer -> Integer -> Value
operator op v1 v2 = convertBack $ v1 `op` v2

compare :: Integer -> Integer -> Value
compare v1 v2
    | v1 < v2   = [Term $ Character '-']
    | v1 > v2   = [Term $ Character '+']
    | otherwise = [Term $ Character '=']

divmod :: Integer -> Integer -> Value
divmod v1 v2 = (Structure $ convertBack quotient) : convertBack remainder where
    quotient = v1 `div` v2
    remainder = v1 `mod` v2

takeArgs :: Expression Symbol -> Interpreter (Integer, Integer)
takeArgs (Structure expr : ts)           = convertArgs expr ts
takeArgs (Term (Character '-') : t : ts) = convertArgs [Term $ Character '-', t] ts
takeArgs (Term (Character '+') : t : ts) = convertArgs [t] ts
takeArgs (t : ts)                        = convertArgs [t] ts
takeArgs _                               = throwError $ Error "Invalid arguments' format"

convertArgs :: Expression Symbol -> Expression Symbol -> Interpreter (Integer, Integer)
convertArgs expr1 expr2 = do
    v1 <- convert expr1
    v2 <- convert expr2
    return (v1, v2)

convert :: Expression Symbol -> Interpreter Integer
convert []                             = throwError $ Error "Empty argument"
convert (Term (Character '-') : terms) = fmap (\x -> -x) $ convertUnsigned terms
convert (Term (Character '+') : terms) = convertUnsigned terms
convert terms                          = convertUnsigned terms

convertUnsigned :: Expression Symbol -> Interpreter Integer
convertUnsigned = fmap fst . foldr op (return (0, 0)) where
    op :: Term Symbol -> Interpreter (Integer, Integer) -> Interpreter (Integer, Integer)
    op (Term (Number n)) v = fmap (\(result, offset) -> (result + n * 2^offset, offset + 32)) v
    op _ _                 = throwError $ Error "Invalid argument: NaN"

convertBack :: Integer -> Expression Symbol
convertBack n | n < 0     = Term (Character '-') : convertBackUnsigned (-n)
              | otherwise = convertBackUnsigned n

convertBackUnsigned :: Integer -> Expression Symbol
convertBackUnsigned n
    | n == 0 = [Term $ Number 0]
    | otherwise = unfoldr op n where
        m = 0x100000000 :: Integer
        op remain | remain > 0 = Just (Term $ Number $ remain `mod` m, remain `div` m)
                  | otherwise  = Nothing
