module RefalAst where

import Control.Monad.Except
import Control.Monad.Reader

type Scope a = [(Name, a)]

type Value = Expression Symbol

type Interpreter = ExceptT Error (Reader (Scope Function, Scope Value))

data Error = Error String
           | UndefinedFunction Name
           | UndefinedVariable Name
           | NotExhaustiveMatching
           | NotMatches
           deriving (Show, Eq)

data Function = BuiltIn (Expression Symbol -> Interpreter Value)
              | Defined Block

type Block = [Sentence]
type Name = String

data Sentence = Sentence (Expression Unbound) [Condition] Substitution
    deriving Show

data Condition = Condition (Expression Evaluable) (Expression Unbound)
    deriving Show

data Substitution = ExpressionSubstitution (Expression Evaluable)
                  | BlockSubstitution Block (Expression Evaluable)
                  deriving Show

type Expression a = [Term a]

data Term a = Term a
            | Structure (Expression a)
            deriving (Show, Eq)

data Unbound = Unbound Symbol
             | UnboundVariable Name Type
             deriving Show

data Evaluable = Evaluable Unbound
               | FunctionApplication Name (Expression Evaluable)
               deriving Show

data Symbol = Character Char
            | Identifier String
            | Number Integer
            | RealNumber Double
            deriving (Show, Eq)

data Type = SymbolType | TermType | ExpressionType
    deriving (Show, Eq)
