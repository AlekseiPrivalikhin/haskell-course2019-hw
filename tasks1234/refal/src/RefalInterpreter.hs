module RefalInterpreter (interpret, test) where

import RefalAst
import RefalArithmetics (arithmeticFuns)

import Control.Monad.Except
import Control.Monad.Reader

import Data.List (partition)

test :: Scope Function -> [Bool]
test scope = fmap helper entryFuns where
    (entryFuns, funs) = split "Test" scope

    startExpr = [Term $ FunctionApplication "Test" []]
    startResExpr = [Term $ FunctionApplication "Test" [Term $ Evaluable $ Unbound $ Identifier "result"]]

    helper fun = case runExpr (fun : funs) startResExpr of
        Left _ -> False
        a      -> a == runExpr (fun : funs) startExpr

interpret :: Scope Function -> [Either Error Value]
interpret scope = fmap helper entryFuns where
    (entryFuns, funs) = split "Go" scope

    startExpr = [Term $ FunctionApplication "Go" []]

    helper fun = runExpr (fun : funs) startExpr

runExpr :: Scope Function -> Expression Evaluable -> Either Error Value
runExpr funs expr = runReader (runExceptT $ eval expr) (funs, [])

split :: Name -> Scope Function -> (Scope Function, Scope Function)
split name = fmap (++ arithmeticFuns) . partition (\(fname, _) -> fname == name)

class Interpretable a where
    eval :: a -> Interpreter Value

instance Interpretable a => Interpretable [a] where
    eval expr = fmap concat $ sequence $ map eval expr

instance Interpretable a => Interpretable (Term a) where
    eval (Term a) = eval a
    eval (Structure expr) = fmap ((:[]) . Structure) $ eval expr

instance Interpretable Evaluable where
    eval (Evaluable unbound)             = eval unbound
    eval (FunctionApplication name expr) = do
        fun <- getFun name
        arg <- eval expr
        case fun of
            BuiltIn f   -> f arg
            Defined blk -> chngVals (evalBlock blk arg) (const [])

instance Interpretable Unbound where
    eval (Unbound symbol)         = return [Term symbol]
    eval (UnboundVariable name _) = getVal name

instance Interpretable Substitution where
    eval (ExpressionSubstitution expr) = eval expr
    eval (BlockSubstitution blk expr)  = eval expr >>= evalBlock blk

evalBlock :: Block -> Expression Symbol -> Interpreter Value
evalBlock [] _ = throwError NotExhaustiveMatching
evalBlock (Sentence pattern conds subst : ss) arg = try `catchError` catcher where
    try = match pattern arg >>= chngVals (check conds >> eval subst) . const

    catcher NotMatches = evalBlock ss arg
    catcher e          = throwError e

check :: [Condition] -> Interpreter ()
check [] = return ()
check (Condition expr pattern : conds) = eval expr >>= match pattern >> check conds

match :: Expression Unbound -> Expression Symbol -> Interpreter (Scope Value)
match [] [] = fmap snd $ lift ask
match (Term (Unbound us) : uts) (Term s : ts)
    | us == s   = match uts ts
    | otherwise = throwError NotMatches
match (Structure expr1 : uts) (Structure expr2 : ts) = match expr1 expr2 >>= chngVals (match uts ts) . (++)
match (Term (UnboundVariable vname vtype) : uts) ts = try `catchError` catcher where
    try = getVal vname >>= helper ts >>= match uts

    helper [] [] = return [] :: Interpreter Value
    helper (x:xs) (y:ys)
        | x == y    = helper xs ys
        | otherwise = throwError NotMatches
    helper xs [] = return xs
    helper _ _   = throwError NotMatches

    catcher (UndefinedVariable _) = bindThenMatch vtype vname uts ts
    catcher e                     = throwError e
match _ _ = throwError NotMatches

bindThenMatch :: Type -> Name -> Expression Unbound -> Expression Symbol -> Interpreter (Scope Value)
bindThenMatch ExpressionType vname uts ts = helper [] ts where
    helper cur remain = try `catchError` catcher where
        try = chngVals (match uts remain) ((vname, cur) :)
        catcher :: Error -> Interpreter (Scope Value)
        catcher _ = case remain of
            [] -> throwError NotMatches
            _  -> helper (cur ++ [head remain]) (tail remain)
bindThenMatch vtype vname uts ts = case ts of
        []   -> throwError NotMatches
        t:remain -> case vtype of
            TermType -> chngVals (match uts remain) ((vname, [t]) :)
            _        -> case t of
                Term _ -> chngVals (match uts remain) ((vname, [t]) :)
                _      -> throwError NotMatches

getFun :: Name -> Interpreter Function
getFun name = do
    (funs, _) <- lift ask
    case lookup name funs of
        Just fun -> return fun
        Nothing  -> throwError $ UndefinedFunction name

getVal :: Name -> Interpreter Value
getVal name = do
    (_, vals) <- lift ask
    case lookup name vals of
        Just val -> return val
        Nothing  -> throwError $ UndefinedVariable name

chngVals :: Interpreter a -> (Scope Value -> Scope Value) -> Interpreter a
chngVals ipr chng = mapExceptT (local (fmap chng)) ipr
